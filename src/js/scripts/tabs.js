function tabs() {
    $(document).on('click', '[data-toggle="tab"]', function(e) {
        e.preventDefault();

        var $this = $(this),
            $nav = $this.closest('.js-tab-nav'),
            $tabContent = $this.closest('.js-tab-container').find('.js-tab-content').eq(0),
            id = $this.attr('href');

        $nav.find('.js-tab-nav-item').removeClass('_active');
        $(this).closest('.js-tab-nav-item').addClass('_active');

        showTab($(id), $tabContent);
    });

    $(document).on('change', '.tab-select select', function() {
        var $this = $(this),
            $target = $($this.find('option:selected').attr('value')),
            $tabContent = $this.closest('.js-tab-container').find('.js-tab-content').eq(0);

        showTab($target, $tabContent);
    });
};

function showTab($target, $tabContent) {
    $tabContent.find('> .js-tab-pane').removeClass('_active');
    $target.delay(1).addClass('_active');
}