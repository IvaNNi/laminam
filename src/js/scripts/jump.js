function jump() {
    if ($(window).width() < 640) {
        return false;
    }

	$(document).on('click.links', '._js-link', function (e) {
        e.preventDefault();
        animationJump($(this).attr('href'));
    });

    function animationJump(link, changeHistoryState = true) {
        if (link.indexOf('#') === -1) {
            $.ajax({
                url: link,
                cache: false,
                success: function(html) {
                    APP.ytPlayers = [];

                    var columns = new TimelineMax(),
                        title = $(html).filter('#title').text(),
                        wrapper = $(html).filter('#wrapper').children();

                    if (changeHistoryState) {
                        window.history.pushState({ 'pageTitle': title }, '', link);
                    }

                    $('#title').text(title);

                    $('.loader-page__content').html(wrapper.clone());
                    $('.loader-page__content').find('.site-header').css({
                        paddingRight: APP.scrollbarWidth
                    });

                    lazyLoadImg();
                    lazyLoadBackgroundImages();
                    loadYoutubeScript();
                    loadNewPage();

                    function loadNewPage() {
                        $('#wrapper').html(wrapper.clone());

                        animationAfterJump();
                        youtubeInit();
                        lazyLoadImg();
                        lazyLoadBackgroundImages();
                        slider();
                        selectize();
                        animateCollections();
                        collectionsSlider();
                        collectionCharacters();
                        validateForm();
                        form();
                        map();
                        projects();
                        setsOnVue();
                        animateScrollMagic();

                        $('.select').styler();

                        $('body')
                            .removeClass('_no-scroll')
                            .css({
                                paddingRight: 0
                            });

                        $('.site-header').css({
                            paddingRight: 0
                        });

                        $(window).scrollTop(0);

                        if(window.location.pathname === '/' || window.location.pathname === 'index.html' || window.location.pathname === 'index.php') {
                            $('body').removeClass('page');
                        } else {
                            $('body').addClass('page');
                        }
                    };

                }
            });
        }
    }

    $(window).on('popstate', function(e) {
        animationJump(window.location.pathname, false);
    });
}