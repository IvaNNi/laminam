function map() {
    $(document).on('click', '._js-change-coordinates', function() {
        var $this = $(this),
            coordinates = $this.data('coordinates').split(','),
            text = $this.data('description'),
            resultCords = [parseFloat(coordinates[0]), parseFloat(coordinates[1])];

        APP.placemark.geometry.setCoordinates(resultCords);
        APP.map.setCenter(resultCords);
        APP.map.container.fitToViewport();
        $('._js-map-description').text(text);
    });

    if($(document).find('#map_area').length) {
        ymaps.ready(function () {
            APP.map = new ymaps.Map('map_area', {
                    center: [55.751574, 37.613856],
                    zoom: 12,
                    controls: []
                }, {
                    searchControlProvider: 'yandex#search'
                });

            var ZoomLayout = ymaps.templateLayoutFactory.createClass("<div>" +
                    "<div id='zoom-in' class='zoom-in'>+</div>" +
                    "<div id='zoom-out' class='zoom-out'>-</div>" +
                    "</div>", {

                    build: function () {
                        ZoomLayout.superclass.build.call(this);

                        this.zoomInCallback = ymaps.util.bind(this.zoomIn, this);
                        this.zoomOutCallback = ymaps.util.bind(this.zoomOut, this);

                        $('#zoom-in').bind('click', this.zoomInCallback);
                        $('#zoom-out').bind('click', this.zoomOutCallback);
                    },

                    clear: function () {
                        $('#zoom-in').unbind('click', this.zoomInCallback);
                        $('#zoom-out').unbind('click', this.zoomOutCallback);

                        ZoomLayout.superclass.clear.call(this);
                    },

                    zoomIn: function () {
                        APP.map= this.getData().control.getMap();
                        APP.map.setZoom(APP.map.getZoom() + 1, {checkZoomRange: true});
                    },

                    zoomOut: function () {
                        APP.map= this.getData().control.getMap();
                        APP.map.setZoom(APP.map.getZoom() - 1, {checkZoomRange: true});
                    }
                }),
                zoomControl = new ymaps.control.ZoomControl({options: {layout: ZoomLayout}});

            APP.map.controls.add(zoomControl);

            APP.placemark = new ymaps.Placemark(APP.map.getCenter(), {}, {
                iconLayout: 'default#image',
                iconImageHref: '/local/templates/laminam/html/build/assets/svg_icons/map-marker.svg',
                iconImageSize: [41, 52],
                iconImageOffset: [-20, -26]
            });

            APP.map.geoObjects
                .add(APP.placemark)
        })
    }
}