function validateForm() {
    var validateOpts = {
        errorClass: '_error',
        errorPlacement: function(error, element) {
            var $formGroup = element.closest('.form-group');

            if ($formGroup.length) {
                $formGroup.addClass('_error');
            }
        },
        unhighlight: function (element) {
          $(element).closest('.form-group').removeClass('_error'); 
        },
        submitHandler: function(form, event) {
            event.preventDefault();

            if ($(form).is('.js-validate-form')) {

                $.ajax({
                    url: $(form).data('ajax-url'),
                    data: $(form).serialize(),
                    dataType: 'json',
                    success: function(resp) {
                        if (resp.res == true) {
                            form.reset();

                            if ($('.setsEmptyPopup').length) {
                                $.magnificPopup.open({
                                    items: {
                                        src: $('.setsEmptyPopup')
                                    },
                                    mainClass: 'mfp-fade',
                                    type: 'inline'
                                });
                            }
                        } else {
                            alert(resp.error);
                        }
                        
                    }
                });

            }

            if ($(form).is('.js-design-form')) {
                var formData = new FormData(form);

                $.ajax({
                    url: $(form).data('ajax-url'),
                    data: formData,
                    method: 'POST',
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function(resp) {
                        if (resp.res == true) {
                            form.reset();

                            $(form).find('.js-load-file-label').text('Прикрепить файл');

                            if ($('.setsEmptyPopup').length) {
                                $.magnificPopup.open({
                                    items: {
                                        src: $('.setsEmptyPopup')
                                    },
                                    mainClass: 'mfp-fade',
                                    type: 'inline'
                                });
                            }
                        } else {
                            alert(resp.error);
                        }
                        
                    }
                });

            }
        }
    }

    $('.js-validate-form').validate(validateOpts);
    $('.js-design-form').validate(validateOpts);
}