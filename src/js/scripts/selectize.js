function selectize() {
    if ($('.selectize').length) {
        var $select = $('.selectize').selectize({
            create: false,
            preload:true,
            delimiter: ',',
            closeAfterSelect: true,
            persist: false,
            onInitialize: function () {
                if ($(this.$control).hasClass('full')) {
                    $(this.$control).closest('.selectize-control').addClass('_active');
                }
            },
            onFocus: function () {
                $(this.$control).closest('.selectize-control').addClass('_active');
            },
            onBlur: function () {
                if (!$(this.$control).hasClass('full')) {
                    $(this.$control).closest('.selectize-control').removeClass('_active');
                }
            }
        });

        var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

        $('.selectize-input input').attr('readonly','readonly');

        if (iOS) {
            $('body').on('click touchend', '.option', function(e) {
                if (!$(this).closest('[data-selectize-id]').length)  {
                    return;
                }

                setTimeout(() => {
                    $select[Number($(this).closest('[data-selectize-id]').data('selectize-id')) - 1].selectize.setValue($(this).text(), false);
                }, 0);
            });
        }

        $(document).on('click', '.js-reset-filter', function() {

            $select.filter('.js-filter-selectize').each(function() {
               this.selectize.clear();

               $(this.selectize.$control).closest('.selectize-control').removeClass('_active');
            });
            $(this).removeClass("active");
        });
    }
}