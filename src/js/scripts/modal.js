function modal() {
    $(document).on('click', '.modal-close', function() {
        var $this = $(this),
            $modal = $this.closest('.modal');

        $modal.removeClass('_in');
        $('body').removeClass('_no-scroll').css({
            paddingRight: 0
        })
        $('.site-header').css({
            paddingRight: 0
        });
        stopAllVideo();
    });

    $(document).on('click', '[data-toggle="modal"]', function(e) {
        var $this = $(this),
            $modal = $($this.data('target'));

        e.preventDefault();
        $modal.addClass('_in');
        $('body').addClass('_no-scroll').css({
            paddingRight: APP.scrollbarWidth
        });
        $('.site-header').css({
            paddingRight: APP.scrollbarWidth
        });
    });

    $(document).mouseup('mouseup touched', function(e) {
        var $el = $('.modal-dialog');
        if (!$el.is(e.target) &&
            $el.has(e.target).length === 0) {
            if ($('.modal').hasClass('_in')) {
                $('.modal').removeClass('_in');
                $('body')
                    .removeClass('_no-scroll')
                    .css({
                        paddingRight: 0
                    });
                $('.site-header').css({
                    paddingRight: 0
                });
                stopAllVideo();
            }
        }
    });
}