function form() {
    $('.form-control')
        .each(function () {
            if ($(this).val()) {
                $(this).closest('.form-group').addClass('_active');
            }
        })
        .on('blur', function() {
            if (!$(this).val()) {
                $(this).closest('.form-group').removeClass('_active');
            }
        })
        .on('focus', function() {
            $(this).closest('.form-group').addClass('_active')
        })
        .on('change', function(){
            var el = $(this);
            if (el.val()) {
                el.addClass('_active');
                el.closest('.form-group').addClass('_active');
            } else {
                el.removeClass('_active');
                el.closest('.form-group').removeClass('_active');
            }
        });

    $('.phone').mask('(000) 00-00-000');
};