function setsOnVue() {
    const storageName = 'cart';

    class Cart {
        constructor() {
            this.items = [];
            this.load()
        }

        save() {
            window.localStorage.setItem(storageName, JSON.stringify(this.items));
        }

        load() {
            this.items = JSON.parse(window.localStorage.getItem(storageName) || '[]');
        }

        createNewSet(name) {
            this.items.push({ name: name, isEdit: false, ids: [] })
            this.save();
        }

        removeSet(index) {
            this.items.splice(index, 1)
            this.save();
        }

        removeItemFromSetByIndex(currentSetIndex, item_id) {
            if (!this.items[currentSetIndex]) {
                return;
            }
            for (let i in this.items[currentSetIndex].ids) {

                if (this.items[currentSetIndex].ids[i] == item_id) {
                    this.items[currentSetIndex].ids.splice(i, 1);
                    break;
                }
            }
            this.save()
        }



        addToCart(setName, id) {
            let isDefineFlag = false;

            for (let item of this.items) {
                if (item.name == setName) {
                    isDefineFlag = true;
                    item.ids.push(id);
                    break;
                }
            }

            if (isDefineFlag == false) {
                this.createNewSet(setName);
                return this.addToCart(setName, id);
            }

            this.save();
        }
    }

    let $card = new Cart();

    if (document.querySelector('#app')) {

        var cart = new Vue({
            delimiters: ['${', '}'],
            el: '#app',
            data: {
                name: '',
                newSet: false,
                create: false,
                items: [],
                dataSetIndex: 1000,
                showPopupFirstTime: 1,
                imgUrl: '/assets/catalogue/bianco.jpg',
                itemsSet: [],
            },

            mounted: function() {

                let flag = window.localStorage.getItem('showPopupFirstTime') || 1;

                if (flag == 1) {
                    $.magnificPopup.open({
                        items: {
                            src: '.setsEmptyPopup',
                            type: 'inline'
                        },
                        mainClass: 'mfp-fade',
                        removalDelay: 300,
                        closeOnContentClick: true,
                        callbacks: {
                            beforeOpen: () => {
                                jQuery('html').addClass('noScrollMob');
                            },
                            beforeClose: () => {
                                jQuery('html').removeClass('noScrollMob');
                            }
                        }
                    });

                    window.localStorage.setItem('showPopupFirstTime', 0);
                }

                this.$el.classList.add('showVueBlock');
                this.items = $card.items.map((s) => {
                    s.thumbs = '';
                    return s;
                });

                let thumbs = [];
                this.items.forEach((s) => {
                    if (s.ids.length) {
                        thumbs.push(s.ids[s.ids.length - 1]);
                    }
                })

                fetch('/local/templates/laminam/ajax/ajax_for_mix.php?ids=' + thumbs.join(','), {
                    method: 'GET',
                    mode: 'cors'
                }).then((response) => {
                    return response.json();
                }).then((json_response) => {
                    let key = 0;
                    this.items.forEach((s, index) => {
                        if (s.ids.length && json_response[key]) {
                            this.setThumbs(index, json_response[key].image)
                            key++;
                        }
                    })

                });
            },

            methods: {
                setThumbs: function(index, image) {
                    this.$nextTick(() => {
                        this.items[index].thumbs = image;
                    })

                },

                setFocus: function() {
                    this.newSet = !this.newSet;
                    Vue.nextTick().then(() => this.$refs.createName.focus());
                },

                createNewSet: function() {

                    $card.createNewSet(this.name);
                    this.items = $card.items;
                    this.name = '';
                    this.newSet = false;
                    this.save();
                },

                removeSet: function(index) {
                    $card.removeSet(index);
                    this.items = $card.items;
                },

                setEdit: function(index) {
                    this.items[index].isEdit = true;
                    this.newSet = !this.newSet;
                    Vue.nextTick().then( () => {
                    	this.$refs.editName[0].focus();
                    })
                    
                    // Vue.nextTick().then(() => this.$refs.editName.focus());
                },

                editSet: function(index) {
                    this.items[index].isEdit = false;
                    $card.items = this.items;
                    $card.save();


                },
                save: function() {
                    window.localStorage.setItem(storageName, JSON.stringify(this.items));
                },
                load: function() {
                    this.items = JSON.parse(window.localStorage.getItem(storageName) || '[]');
                },
                addToSet: function(setName, id) {

                },


                // popup if delete set
                openPopup: function(index) {

                    this.dataSetIndex = index;

                    $('.setsDelete').magnificPopup({
                        items: {
                            src: '.setsDeletePopup',
                            type: 'inline'
                        },
                        mainClass: 'mfp-fade',
                        removalDelay: 300,
                        callbacks: {
                            beforeOpen: () => jQuery('html').addClass('noScrollMob'),
                            beforeClose: () => jQuery('html').removeClass('noScrollMob')
                        }
                    }).click();
                },


                approveRemove: function() {
                    this.removeSet(this.dataSetIndex);
                    $.magnificPopup.instance.close();
                }
            }
        });
    }


    // Cart item



    if (document.querySelector('#appItem')) {

        var cartItem = new Vue({
            delimiters: ['${', '}'],
            el: '#appItem',
            data: {
                dataSetIndex: 1000,
                currentSetIndex: -1,
                test: 'Vue',
                items: []
            },


            mounted: function() {
                this.currentSetIndex = parseInt(window.location.search.substr(1));
                let ids = $card.items[this.currentSetIndex] ? $card.items[this.currentSetIndex].ids : [];
                
                fetch('/local/templates/laminam/ajax/ajax_for_mix.php?ids=' + ids.join(','), {
                    method: 'GET',
                    mode: 'cors'
                }).then((response) => {
                    return response.json();
                }).then((json_response) => {
                    this.items = json_response;

                });

            },

            methods: {

                removeSet: function(id) {

                    for (let i in this.items) {

                        if (this.items[i].id == id) {
                            this.items.splice(i, 1);
                            break;
                        }
                    }

                    $card.removeItemFromSetByIndex(this.currentSetIndex, id)
                    $card.save();

                },

                // popup if delete 
                openPopup: function(id) {

                    this.dataSetIndex = id;

                    $('.setsDelete').magnificPopup({
                        items: {
                            src: '.setsDeletePopup',
                            type: 'inline'
                        },
                        mainClass: 'mfp-fade',
                        removalDelay: 300,
                        callbacks: {
                            beforeOpen: () => jQuery('html').addClass('noScrollMob'),
                            beforeClose: () => jQuery('html').removeClass('noScrollMob')
                        }
                    }).click();
                },

                // popup if delete set
                openPopupSend: function(index) {

                    this.dataSetIndex = index;

                    $('.setsSingle').magnificPopup({
                        items: {
                            src: '.setsSinglePopup',
                            type: 'inline'
                        },
                        mainClass: 'mfp-fade',
                        removalDelay: 300,
                        callbacks: {
                            beforeOpen: function() {
                                jQuery('html').addClass('noScrollMob');
                            },
                            beforeClose: function() {
                                jQuery('html').removeClass('noScrollMob');
                            }
                        }
                    }).click();
                },

                approveRemove: function() {
                    this.removeSet(this.dataSetIndex);
                    $.magnificPopup.instance.close();
                }

            }
        });
    }


    if (document.querySelector('#appCatalog')) {

        var cartItem = new Vue({
            delimiters: ['${', '}'],
            el: '#appCatalog',
            data: {
                newSet: false,
                create: false,
                test: 'Vue',
                name: '',
                newName: '',
                items: [],
                id: -1,
                hideList: false,
                dataSetIndex: 1000
            },
            mounted: function() {
                this.id = parseInt(document.querySelector('[data-id]').getAttribute('data-id'));

                this.items = $card.items.map((s) => {
                    s.disabled = (s.ids.indexOf(this.id) > -1) ? true : false;
                    return s;


                });


            },
            methods: {
                createNewSet: function() {

                	if(this.name){
                    $card.createNewSet(this.name);
                    this.items = $card.items;
                    $card.addToCart(this.name, this.id);
                    this.newName = this.name;
                    event.target.blur();
                    this.name = '';

                	}

                },


                addToSet: function(item, setIndex) {
                    $card.addToCart(item.name, this.id);
                    item.disabled = true;
                    this.newName = item.name;
                },


                openPopupSend: function(index) {

                    this.dataSetIndex = index;

                    $('.setsSingle').magnificPopup({
                        items: {
                            src: '.setsSinglePopup',
                            type: 'inline'
                        },
                        mainClass: 'mfp-fade',
                        removalDelay: 300,
                        callbacks: {
                            beforeOpen: function() {
                                jQuery('html').addClass('noScrollMob');
                            },
                            beforeClose: function() {
                                jQuery('html').removeClass('noScrollMob');
                            }
                        }
                    }).click();
                }

            }

        });
    }

    return $card;

}


