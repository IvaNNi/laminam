$(function() {
    // Mobile menu
    $(document).on('click', '._js-toggle-menu', function() {
        $('.site-mobile').toggleClass('_active');

        if (!$('.site-mobile').hasClass('_active')) {
            $('body')
                .removeClass('noScrollMob')
                .css({
                    paddingRight: 0
                });
        } else {
            $('body')
                .addClass('noScrollMob')
                .css({
                    paddingRight: APP.scrollbarWidth
                });
        }
    });

    $(document).on('mouseup touched', function(e) {
        var $el = $('.site-mobile__inner');
        if (!$el.is(e.target) &&
            $el.has(e.target).length === 0) {
            if ($('.site-mobile').hasClass('_active')) {
                $('.site-mobile').removeClass('_active');
                $('body')
                    .removeClass('noScrollMob')
                    .css({
                        paddingRight: 0
                    });
            }
        }
    });

    $(document).on('click', '._js-animate-video', function() {
        $(this).closest('.info-block__video-container').addClass('_active');
    });

    var videoHeight = null;

    $(document).on('click', '._js-video-block', function() {
        var $this = $(this);
        var video = $this.find('._js-video')[0];

        $this.toggleClass('_played');
        $this.find('._js-video-content').toggleClass('is-hidden');

        if (video.paused) {
            var body = $('html, body');
            videoHeight = $this.height();

            video.play();
        } else {
            video.pause();
        }
    });

    $(document).on('click', '.js-extend-filter', function() {
        var fadeDuration = 300;

        $(this).toggleClass('is-filter');

        $('.js-collections-slider-wrap').toggleClass('is-active');
        $('.js-catalog-filter-wrap').toggleClass('is-active');
    });

    $(document).on('change', '[data-collections-filter]', function() {
        var emptyFilters = true;
        var $collectionsList = $('.js-collections-list');
        var $materialsList = $('.js-materials-list');
        var activeCls = 'is-active';
        var emptyCls = 'is-empty';
        var filterReset = $(".js-reset-filter");

        var $materialsItem = $('[data-materials-item]');
        var filterParams = {};

        //показываем кнопку сброса
        filterReset.addClass("active");

        $('[data-collections-filter]').each(function () {
            var $filter = $(this);

            if ($filter.val() !== '') {
                filterParams[$filter.data('collections-filter')] = $filter.val();
                emptyFilters = false;
            }

            if (emptyFilters && $collectionsList.length) {
                $collectionsList.addClass(activeCls);
                $materialsList.removeClass(activeCls + ' ' + emptyCls);
            } else {
                $collectionsList.removeClass(activeCls);
                $materialsList.addClass(activeCls);

                var $materialsItems = $('[data-materials-item]');
                var materialsItemsLength = $materialsItems.length;

                $materialsItems.each(function() {
                    var $this = $(this);
                    var isHidden = false;

                    for (var i in filterParams) {
                        if ((!Array.isArray($this.data('materials-item')[i]) && $this.data('materials-item')[i] !== filterParams[i]) ||
                            (Array.isArray($this.data('materials-item')[i]) && !$this.data('materials-item')[i].includes(filterParams[i]))) {
                            isHidden = true;
                        }
                    }

                    $this[isHidden ? 'addClass' : 'removeClass']('is-hidden');
                });

                $('.js-materials-list')[$materialsItems.filter('.is-hidden').length < materialsItemsLength ? 'removeClass' : 'addClass'](emptyCls);
            }
        });
    });

    $(document).on('click', '.js-collections-list-btn', function () {
        $(this) .closest('.js-collections-list').addClass('show-all');
    });

    $(document).on('click', '._js-stop-video', function() {
        var idVideoEl = $(this).closest('.video-block').find('.video, .video-frame').attr('id');

        $(this).closest('.video-block').removeClass('_played _active');
        for (var ind = 0; ind < APP.ytPlayers.length; ind++) {
            if (APP.ytPlayers[ind].a.id == idVideoEl) {
                stopYTVideo(APP.ytPlayers[ind]);
                break;
            }
        }
    });

    $(document).on('click', '._js-play-yt-video', function() {
        var idVideoEl = $(this).closest('.video-block').find('.video, .video-frame').attr('id');

        $(this).closest('.video-block').addClass('_played');
        for (var ind = 0; ind < APP.ytPlayers.length; ind++) {
            if (APP.ytPlayers[ind].a.id == idVideoEl) {
                playYTVideo(APP.ytPlayers[ind]);
                break;
            }
        }
    });

    $(document).on('click', '._js-stop-yt-video', function() {
        var idVideoEl = $(this).closest('.video-block').find('.video, .video-frame').attr('id');

        $(this).closest('.video-block').removeClass('_played _active');
        for (var ind = 0; ind < APP.ytPlayers.length; ind++) {
            if (APP.ytPlayers[ind].a.id == idVideoEl) {
                stopYTVideo(APP.ytPlayers[ind]);
                break;
            }
        }
    });

    var imgData = $('[data-stove-params]').first().data('stove-params');

    $(document).on('change', '[data-stove-params]', function () {
        var $this = $(this);
        imgData = $this.data('stove-params');

        $('.js-load-file').val('');
        $('.js-load-file-label').text('Прикрепить файл');

        updateImgData('.js-data-img-src', '.js-data-img-width', '.js-data-img-height', '.js-data-img-size', imgData);
    });

    function updateImgData(imgSrcElem, imgWidthElem, imgHeightElem, imgSizeElem, data) {
        $(imgSrcElem).attr('src', data.src);
        $(imgWidthElem).text(data.width);
        $(imgHeightElem).text(data.height);
        $(imgSizeElem).text(data.size);
    }

    var URL = window.URL || window.webkitURL;

    $(document).on('change', '.js-load-file', function() {
        var $this = $(this);

        if (!window.FileReader) {
            alert('Загрузка файлов не поддерживается вашим браузером');

            return;
        }

        var file = $this[0].files[0];
        var fileSize = file.size / 1024 / 1024;

        if (fileSize > imgData.size) {
            alert('Файл больше ' + imgData.size + ' мб');
            $this.val('');
            return;
        }

        if (file.name.split('.').pop().toLowerCase() !== 'tif') {
            var image = new Image();

            image.onload = function() {
                if (this.width > imgData.width || this.height > imgData.height) {
                    alert('Размер до ' + imgData.width + 'x' + imgData.height + ' px');
                    $this.val('');
                    return;
                }

                $('.js-load-file-label').text(file.name);
            };

            image.src = URL.createObjectURL(file);
        } else {
            $('.js-load-file-label').text(file.name);
        }
    });

    $('.select').styler();

    tabs();
    form();
    modal();
    map();
    slider();
    selectize();
    animateCollections();
    collectionsSlider();
    collectionCharacters();
    validateForm();
    projects();
    setsOnVue();
    jump();
});

lazyLoadImg();
lazyLoadBackgroundImages();

 function loadYoutubeScript() {
    if (typeof(YT) == 'undefined' || typeof(YT.Player) == 'undefined') {
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }
}

window.onYouTubePlayerAPIReady = function() {
    youtubeInit();
};

setTimeout(function() {
    loadYoutubeScript();
}, 2500);

$(window).on('load', function() {
    $('.js-site-preloader').addClass('is-hidden').on('transitionend', function() {
        requestAnimationFrame(animationForBlock);
        requestAnimationFrame(animateScrollMagic);
        requestAnimationFrame(function() {
            $('.animated._now').each(function() {
                var animationType = $(this).data('animate');

                $(this).addClass(animationType);
            });
        });
    });

    var linksArr = [];

    $('._js-link').not('[target="_blank"]').not('[href="#"]').not('[href=""]').each(function() {

        var href = $(this).attr('href');

        if (linksArr.indexOf(href) < 0 && href.indexOf('catalog') === -1) {
            linksArr.push(href);
        }
    });

    console.log(linksArr);

    var pattern = /(\/upload\/)([/|.|\w|\s|-])*\.(?:jpg|gif|png)/gmi;

    function loadPictures(key) {

        $.ajax({
            url: linksArr[key],
            success: function(html) {
                var m;

                while ((m = pattern.exec(html)) !== null) {
                    if(m[0]) {
                        var img = new Image();
                        img.src = m[0];
                    }
                }

                if (key < linksArr.length) {
                    loadPictures(++key);
                }
            }
        });
    }

    if (window.matchMedia("(min-width: 768px)").matches) {
           loadPictures(0);
       }
});