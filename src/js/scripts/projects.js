function projects() {



    // slider with popup

    if ($('.project-slider').length) {
        $('.project-slider').slick({
            infinite: true,
            slidesToShow: 1,
            dots: false,
            arrows: true
        }).addClass('sliderVisible');


        var flag = false;
        $('.project-slider').on('afterChange', function(event, slick, currentSlide, nextSlide) {
            flag = true;
            window.currentSlide = currentSlide;
        });

        $('.project-slider__slide').magnificPopup({
            items: {
                src: '#sliderPopup',
                type: 'inline'
            },
            callbacks: {
                open: function() {
                    var slider = $('.project-slider');

                    if (flag) {
                        slider[0].slick.slickGoTo(parseInt(currentSlide), true);
                    } else {
                        slider.slick('unslick').slick()
                    };

                },
                close: function() {
                    $('.project-slider').slick('unslick').slick();
                    var currentSlide = 0;
                    flag = false;
                }
            },
            removalDelay: 300,
            mainClass: 'mfp-fade'
        });

    }


    // popup for send full set
    if ($('.setsFull').length) {
        $('.setsFull').magnificPopup({
            items: {
                src: '.setsFullPopup',
                type: 'inline'
            },

            mainClass: 'mfp-fade',
            removalDelay: 300,
            callbacks: {
                beforeOpen: function() {
                    jQuery('html').addClass('noScrollMob');
                },
                beforeClose: function() {
                    jQuery('html').removeClass('noScrollMob');
                }
            }
        });
    }



    // popup for send set on email
    if ($('.setsEmailPopup').length) {
        $('.setsEmail').magnificPopup({
            items: {
                src: '.setsEmailPopup',
                type: 'inline'
            },
            mainClass: 'mfp-fade',
            removalDelay: 300,
            callbacks: {
                beforeOpen: function() {
                    jQuery('html').addClass('noScrollMob');
                },
                beforeClose: function() {
                    jQuery('html').removeClass('noScrollMob');
                }
            }
        });
    }


    // popup for send single item
    if ($('.setsSinglePopup').length) {
        $('.setsSingle').magnificPopup({
            items: {
                src: '.setsSinglePopup',
                type: 'inline'
            },
            mainClass: 'mfp-fade',
            removalDelay: 300,
            callbacks: {
                beforeOpen: function() {
                    jQuery('html').addClass('noScrollMob');
                },
                beforeClose: function() {
                    jQuery('html').removeClass('noScrollMob');
                }
            }
        });
    }



    // popup if sets is empty
    /*if ($('.setsEmptyPopup').length) {
    	$('#demoPopup').magnificPopup({ //on click demo button
    		  items: {
    		     	src: '.setsEmptyPopup', 
    		     	type: 'inline'
    		  },
    		  mainClass: 'mfp-fade',
    		  removalDelay: 300,
    		  closeOnContentClick: true,
    		  callbacks: {
                    beforeOpen: function() {
                        jQuery('html').addClass('noScrollMob');
                    },
                    beforeClose: function() {
                        jQuery('html').removeClass('noScrollMob');
                    }
                }
    	});
    }*/


    // popup if delete set
    /*$(document).on('click', '.setsDelete', function (){
		$('.setsDelete').magnificPopup({
			  items: {
			     	src: '.setsDeletePopup', 
			     	type: 'inline'
			  },
			  mainClass: 'mfp-fade',
			  removalDelay: 300,
	  		  callbacks: {
	                beforeOpen: function() {
	                    jQuery('html').addClass('noScrollMob');
	                },
	                beforeClose: function() {
	                    jQuery('html').removeClass('noScrollMob');
	                }
	            }
		});
	});*/



    // if ($('.setsDeletePopup').length) {
    //     $('.setsDelete').magnificPopup({
    //         items: {
    //             src: '.setsDeletePopup',
    //             type: 'inline'
    //         },
    //         mainClass: 'mfp-fade',
    //         removalDelay: 300,
    //         callbacks: {
    //             beforeOpen: function() {
    //                 jQuery('html').addClass('noScrollMob');
    //             },
    //             beforeClose: function() {
    //                 jQuery('html').removeClass('noScrollMob');
    //             }
    //         }
    //     });
    // }



    // css class for close current popup
    $(document).on('click', '.closePopup', function() {
        $.magnificPopup.instance.close();
    });


    // Form validation
    $('.form-submit').on('click', function(event) {
    	var r = /^.+@\w+\.\w{2,12}$/i;
    	// var open_popup_link = $(".form-submit-popup");
    	// var open_popup_link_error = $(".form-submit-popup-error");
    	event.preventDefault();
    	var error = [];

    	$.each($(this).closest('form').serializeArray(), function(index, data){
    		if(data.value == ""){//сперва проверка на пустоту
    			$("."+data.name).parent().addClass("_error");
    			error.push(data.name);
    		}else{
    			if(data.name == "mail"){
    				if(!r.test($("[name="+data.name+"]").val())){
    					$("."+data.name).parent().addClass("_error");
    					error.push(data.name);
    				}else{
    					$("."+data.name).parent().removeClass("_error");
    					if(error.indexOf(data.name) != "-1")
    						error.splice(error.indexOf(data.name), 1);
    				}
    			}else if(data.name == "phone") {
    				if ($("[name="+data.name+"]").val().length == 15) {
    					$("."+data.name).parent().removeClass("_error");
    					if(error.indexOf(data.name) != "-1")
    						error.splice(error.indexOf(data.name), 1);
    				} else {
    					$("."+data.name).parent().addClass("_error");
    					if(error.indexOf(data.name) == "-1")
    						error.push(data.name);
    				}

    			} else {
    				$("."+data.name).parent().removeClass("_error");
    				if(error.indexOf(data.name) != "-1")
    					error.splice(error.indexOf(data.name), 1);
    			}
    		}
    	});
    	/*if(error.length == 0){
    		BX.ajax.get(
    			"/ajax/request.php?"+$(this).closest('form').serialize(),
    			function (result)
    			{
    				var new_result = JSON.parse(result);
    				if(new_result.res){
    					open_popup_link.click();
    					$('.form-control').val('');
    					$('.form-group').removeClass('_active');
    				}else{
    					open_popup_link_error.click();
    					// $('.js-text').html(",  Причина:"+new_result.reason);
    				}
    			}
    		);
    	}*/
    });

}


// show message when form submitted
function showSubmitMessage(){
	document.querySelector('.form-message').classList.add('ShowMessage');
}
