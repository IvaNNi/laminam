function slider() {
    $('.slider')
        .on('init', function () {
            $(this).addClass('slick-after-init');
            $(this).closest('.loader-container').addClass('_active');
        })
        .each(function() {
            if(!$(this).hasClass('slick-initialized')) {
                var $this = $(this),
                    $imgHeight = $this.find('img').height(),
                    $loader = $this.closest('.loader-container');

                $loader.css('max-height', $imgHeight);

                if ($this.hasClass('_js-slider-info')) {
                    $this.slick({
                        dots: true,
                        arrows: false,
                        fade: true,
                        autoplay: true,
                        autoplaySpeed: 4000,
                        touchThreshold: 50
                    });
                }

                if ($this.hasClass('_js-slider-catalog')) {
                    $this.slick({
                        appendArrows: '.js-catalog-nav',
                        variableWidth: true,
                        arrows: true,
                        speed: 700,
                        autoplay: true,
                        prevArrow: '<button type="button" class="slick-prev"><svg class="icon icon-arrow-prev"><use xlink:href="#arrow-prev"></use></svg></button>',
                        nextArrow: '<button type="button" class="slick-next"><svg class="icon icon-arrow-next"><use xlink:href="#arrow-next"></use></svg></button>',
                        responsive: [
                            {
                             breakpoint: 767,
                             settings: {
                               dots: true
                             }
                           }
                        ]
                    });

                    $this.find('.slick-slide').on('click', function () {
                        var goTo = $(this).data('go-to');

                        $('._js-slider-gallery').slick('slickGoTo', goTo, true);
                        $('._js-slider-gallery').slick('setPosition');
                    });
                }

                if($this.hasClass('_js-slider-gallery')) {
                    $this.slick({
                        arrows: true,
                        prevArrow: '<button type="button" class="slick-prev"><svg class="icon icon-arrow-prev"><use xlink:href="#arrow-prev"></use></svg></button>',
                        nextArrow: '<button type="button" class="slick-next"><svg class="icon icon-arrow-next"><use xlink:href="#arrow-next"></use></svg></button>',
                    });
                }

                if ($this.hasClass('_js-slider-main-bg')) {
                    $this.slick({
                        dots: false,
                        arrows: false,
                        fade: true,
                        autoplay: true,
                        speed: 1000,
                        autoplaySpeed: 3000
                    });
                }
            }
        });

    $('.slider-item').on('touchStart mousedown', function (e) {
        $(this).addClass('is-grabbing');
    }).on('touchEnd mouseup', function (e) {
        $(this).removeClass('is-grabbing');
    });
}