function collectionsSlider() {
    var $collectionsSlider = $('.js-collections-slider');
    var scrollbarExists = true;

    if ($collectionsSlider.length) {
        var scrollbarOptions = {
            suppressScrollY: true
        };

        // Скроллбар на меню
        var ps = new PerfectScrollbar($collectionsSlider[0], scrollbarOptions);
        var isClicked = null;
        var coordX;
        var currentScrollLeft;

        // Драг меню
        $collectionsSlider.on('mousedown', function(e) {
            e.preventDefault();

            coordX = e.pageX;
            currentScrollLeft = $collectionsSlider[0].scrollLeft;

            isClicked = true;
        });

        $('body').on('mousemove', function(e) {
            if (isClicked) {
                $collectionsSlider[0].scrollLeft = currentScrollLeft - (e.pageX - coordX);
            }
        }).on('mouseup', function() {
            isClicked = false;
        });

        $(window).resize(throttle(collectionsSliderUpdate, 50));
        $(window).trigger('resize');

        function collectionsSliderUpdate() {

            var activeCls = 'is-active';
            var $collectionsSliderBtn = $('.js-collections-slider-btn');

            if ($(window).width() <= 1024 && scrollbarExists) {
                // Дестрой скроллбара
                ps.destroy();
                ps = null;

                scrollbarExists = false;

                return;
            }

            if ($(window).width() > 1024) {

                if (!scrollbarExists) {
                    ps = new PerfectScrollbar($collectionsSlider[0], scrollbarOptions);
                    scrollbarExists = true;
                }

                ps.update();

                if ($collectionsSlider.hasClass('ps--active-x')) {
                    $collectionsSliderBtn.addClass(activeCls);
                } else {
                    $collectionsSliderBtn.removeClass(activeCls);
                }
            }
        }

        // Скролл по навигации
        var isAnimated = false;

        $('.js-collections-slider-btn').on('click', function(e) {
            e.preventDefault();

            if (isAnimated) {
                return;
            }

            var $this = $(this);
            var offset = 0;

            if ($this.hasClass('is-next')) {
                offset = 150;
            } else if ($this.hasClass('is-prev')) {
                offset = -150;
            }

            isAnimated = true;

            $collectionsSlider.stop(true, true).animate({ scrollLeft: $collectionsSlider[0].scrollLeft + offset}, 300, function() {
                isAnimated = false;
            });
        });
    }
}