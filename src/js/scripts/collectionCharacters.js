var charactersTableHeight;
var animationDuration = 600;

function collectionCharacters() {
    if ($('.js-characters-table-wrap').length) {
        $('.js-characters-table-wrap').each(function() {
            var $innerContent = $(this).find('.js-characters-table-wrap-inner');

            // Минус единица здесь костыль, проблемы с округлением
            if ($innerContent.get(0).scrollHeight - 1 > parseInt($innerContent.css('height'))) {
                $(this).find('.js-toggle-characters').addClass('is-active');
            }
        });
    }
}

$(document).on('click', '.js-toggle-characters', function() {
    var $this = $(this);
    var $parent = $this.closest('.js-characters-table-wrap');
    var $innerContent = $parent.find('.js-characters-table-wrap-inner');

    if (!charactersTableHeight) {
        charactersTableHeight = parseInt($innerContent.css('height'));
    }

    $parent.toggleClass('is-full');

    $innerContent.animate({ height: (parseInt($innerContent.height()) > charactersTableHeight) ? charactersTableHeight  : $innerContent.get(0).scrollHeight }, animationDuration);
});

$(document).on('click', '.js-toggle-stove-combinations', function() {
    var $parent = $(this).closest('.js-stove-combinations-wrap');

    $parent.toggleClass('is-full').find('.js-stove-combinations-wrap-inner').slideToggle(animationDuration, function() {
        $parent.get(0).scrollIntoView({ behavior: 'smooth', block: 'center' });
    });
});
