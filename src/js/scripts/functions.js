// Functions
function scrollbarWidth() {
    var documentWidth = parseInt(document.documentElement.clientWidth);
    var windowsWidth = parseInt(window.innerWidth);
    var scrollbarWidth = windowsWidth - documentWidth;
    return scrollbarWidth;
}

function playYTVideo(e) {
    if (e.playVideo) {
        e.playVideo();
    }
}

function stopYTVideo(e) {
    e.stopVideo();
}

function stopAllVideo() {
    var idVideoEl = $('.video, .video-frame').attr('id');

    $('.video-block').removeClass('_played _active');
    for (var ind = 0; ind < APP.ytPlayers.length; ind++) {
        stopYTVideo(APP.ytPlayers[ind]);
    }
}

function animationForBlock() {
    $('._js-animated-block').each(function() {
        $(this).find('.animated').each(function() {
            var animationType = $(this).data('animate');

            $(this).addClass(animationType);
        });
    });
}

function animationElement() {
    $('.animated').each(function() {
        var animationType = $(this).data('animate');

        if ($(document).scrollTop() + $(window).height() / 1.3 > $(this).offset().top && $(document).scrollTop() - $(this).offset().top < $(this).height()) {
            $(this).addClass(animationType);
        }
    });
}

function animateCollections() {
    $('.js-collections-list-item').each(function(i, elem) {
        var tl = new TimelineLite({ paused: true });

        var $content = $(this).find('.js-collections-list-content');
        var $items = $(this).find('.js-stove-list-item');
        // var $stoveLayer = $(this).find('.js-stove-list-layer');

        // tl.to($content, 0.6, { ease: Power3.easeIn, x: 0 }).to($stoveLayer, 0.4, { height: 0, ease: Linear.easeNone});
        // $items.each(function(index, element) {
            // tl.set(element, { opacity: 0 });
        // });

        tl.to($content, 0.6, { ease: Circ.easeOut, x: 0 }).to($items, 0.35, { ease: Power4.easeOut, css: { opacity: 1, className: '+=is-active' } });

        elem.animation = tl;

        $(elem).on('mouseenter', function() {
            this.animation.play();
        }).on('mouseleave', function () {
            this.animation.reverse();
        });
    });
}

function animationAfterJump() {
    $(window).on('load scroll', function(){
        $(document)
            .find('._js-animated-block').each(function() {
                if ($(document).scrollTop() + $(window).height() / 1.2 > $(this).offset().top && $(document).scrollTop() - $(this).offset().top < $(this).height()) {
                    $(this).find('.animated').each(function() {
                        var animationType = $(this).data('animate'),
                            style = $(this).attr('style');

                        $(this).addClass(animationType).attr('style', (style !== undefined ? style + ';' : '') + '-webkit-animation-delay: 0s; animation-delay: 0s; -webkit-animation-duration: 0s; animation-duration: 0s; opacity: 1');
                    });
                }
            });
    });
}

function lazyLoadImg() {
    $(document).on('ready', function() {
        $('[data-src]').each(function() {
            $(this).data('load', false);
        });
    });

    $(window).on('load scroll', function () {
        loadImg();
    });
}

function lazyLoadBackgroundImages() {
    $('[data-bgi]').each(function() {
        $(this).data('load', false);
    });

    $(window).on('load scroll', function () {
        loadBackgroundImages();
    });
}

function loadImg() {
    $('[data-src]').each(function() {
        var $this = $(this),
            src = $this.data('src');
        if ($(document).scrollTop() + $(window).height() * 1.5 > $this.offset().top && $(document).scrollTop() - $this.offset().top < $this.height() && !$this.data('load')) {
            $this.attr('src', src).data('load', true);
        }
    });
}

function loadBackgroundImages() {
    $('[data-bgi]').each(function() {
        var $this = $(this),
            src = $this.data('bgi');

        if ($(document).scrollTop() + $(window).height() > $this.offset().top && $(document).scrollTop() - $this.offset().top < $this.height() && !$this.data('load')) {
            $this.attr('style', 'background-image: url(' + src + ');').data('load', true);
        }
    });
}

function youtubeInit() {
    if (window.YT !== undefined) {
        $('.video, .video-frame').each(function(i, e) {
            var player = new YT.Player('video_' + $(e).data('video'), {
                height: '360',
                width: '640',
                videoId: $(e).data('video'),
                playerVars: {
                    'autoplay': 0,
                    'showinfo': 0,
                    'rel': 0,
                    'controls': 0,
                    'loop': 0,
                    'enablejsapi': 1,
                    'modestbranding': 0,
                    'playlist': $(e).data('video')
                }
            });
            APP.ytPlayers.push(player);
        });
    }
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            return false;
        }
    }
    return true;
}

function throttle(func, ms) {
    var isThrottled = false,
        savedArgs,
        savedThis;

    function wrapper() {
        if (isThrottled) {
            savedArgs = arguments;
            savedThis = this;
            return;
        }

        func.apply(this, arguments);

        isThrottled = true;

        setTimeout(function() {
            isThrottled = false;

            if (savedArgs) {
                wrapper.apply(savedThis, savedArgs);
                savedArgs = savedThis = null;
            }
        }, ms);
    }

    return wrapper;
}