'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var APP = {
    scrollbarWidth: scrollbarWidth(),
    ytPlayers: [],
    map: {},
    placemark: {}
};
function tabs() {
    $(document).on('click', '[data-toggle="tab"]', function (e) {
        e.preventDefault();

        var $this = $(this),
            $nav = $this.closest('.js-tab-nav'),
            $tabContent = $this.closest('.js-tab-container').find('.js-tab-content').eq(0),
            id = $this.attr('href');

        $nav.find('.js-tab-nav-item').removeClass('_active');
        $(this).closest('.js-tab-nav-item').addClass('_active');

        showTab($(id), $tabContent);
    });

    $(document).on('change', '.tab-select select', function () {
        var $this = $(this),
            $target = $($this.find('option:selected').attr('value')),
            $tabContent = $this.closest('.js-tab-container').find('.js-tab-content').eq(0);

        showTab($target, $tabContent);
    });
};

function showTab($target, $tabContent) {
    $tabContent.find('> .js-tab-pane').removeClass('_active');
    $target.delay(1).addClass('_active');
}
function form() {
    $('.form-control').each(function () {
        if ($(this).val()) {
            $(this).closest('.form-group').addClass('_active');
        }
    }).on('blur', function () {
        if (!$(this).val()) {
            $(this).closest('.form-group').removeClass('_active');
        }
    }).on('focus', function () {
        $(this).closest('.form-group').addClass('_active');
    }).on('change', function () {
        var el = $(this);
        if (el.val()) {
            el.addClass('_active');
            el.closest('.form-group').addClass('_active');
        } else {
            el.removeClass('_active');
            el.closest('.form-group').removeClass('_active');
        }
    });

    $('.phone').mask('(000) 00-00-000');
};
function modal() {
    $(document).on('click', '.modal-close', function () {
        var $this = $(this),
            $modal = $this.closest('.modal');

        $modal.removeClass('_in');
        $('body').removeClass('_no-scroll').css({
            paddingRight: 0
        });
        $('.site-header').css({
            paddingRight: 0
        });
        stopAllVideo();
    });

    $(document).on('click', '[data-toggle="modal"]', function (e) {
        var $this = $(this),
            $modal = $($this.data('target'));

        e.preventDefault();
        $modal.addClass('_in');
        $('body').addClass('_no-scroll').css({
            paddingRight: APP.scrollbarWidth
        });
        $('.site-header').css({
            paddingRight: APP.scrollbarWidth
        });
    });

    $(document).mouseup('mouseup touched', function (e) {
        var $el = $('.modal-dialog');
        if (!$el.is(e.target) && $el.has(e.target).length === 0) {
            if ($('.modal').hasClass('_in')) {
                $('.modal').removeClass('_in');
                $('body').removeClass('_no-scroll').css({
                    paddingRight: 0
                });
                $('.site-header').css({
                    paddingRight: 0
                });
                stopAllVideo();
            }
        }
    });
}
function slider() {
    $('.slider').on('init', function () {
        $(this).addClass('slick-after-init');
        $(this).closest('.loader-container').addClass('_active');
    }).each(function () {
        if (!$(this).hasClass('slick-initialized')) {
            var $this = $(this),
                $imgHeight = $this.find('img').height(),
                $loader = $this.closest('.loader-container');

            $loader.css('max-height', $imgHeight);

            if ($this.hasClass('_js-slider-info')) {
                $this.slick({
                    dots: true,
                    arrows: false,
                    fade: true,
                    autoplay: true,
                    autoplaySpeed: 4000,
                    touchThreshold: 50
                });
            }

            if ($this.hasClass('_js-slider-catalog')) {
                $this.slick({
                    appendArrows: '.js-catalog-nav',
                    variableWidth: true,
                    arrows: true,
                    speed: 700,
                    autoplay: true,
                    prevArrow: '<button type="button" class="slick-prev"><svg class="icon icon-arrow-prev"><use xlink:href="#arrow-prev"></use></svg></button>',
                    nextArrow: '<button type="button" class="slick-next"><svg class="icon icon-arrow-next"><use xlink:href="#arrow-next"></use></svg></button>',
                    responsive: [{
                        breakpoint: 767,
                        settings: {
                            dots: true
                        }
                    }]
                });

                $this.find('.slick-slide').on('click', function () {
                    var goTo = $(this).data('go-to');

                    $('._js-slider-gallery').slick('slickGoTo', goTo, true);
                    $('._js-slider-gallery').slick('setPosition');
                });
            }

            if ($this.hasClass('_js-slider-gallery')) {
                $this.slick({
                    arrows: true,
                    prevArrow: '<button type="button" class="slick-prev"><svg class="icon icon-arrow-prev"><use xlink:href="#arrow-prev"></use></svg></button>',
                    nextArrow: '<button type="button" class="slick-next"><svg class="icon icon-arrow-next"><use xlink:href="#arrow-next"></use></svg></button>'
                });
            }

            if ($this.hasClass('_js-slider-main-bg')) {
                $this.slick({
                    dots: false,
                    arrows: false,
                    fade: true,
                    autoplay: true,
                    speed: 1000,
                    autoplaySpeed: 3000
                });
            }
        }
    });

    $('.slider-item').on('touchStart mousedown', function (e) {
        $(this).addClass('is-grabbing');
    }).on('touchEnd mouseup', function (e) {
        $(this).removeClass('is-grabbing');
    });
}
function selectize() {
    if ($('.selectize').length) {
        var $select = $('.selectize').selectize({
            create: false,
            preload: true,
            delimiter: ',',
            closeAfterSelect: true,
            persist: false,
            onInitialize: function onInitialize() {
                if ($(this.$control).hasClass('full')) {
                    $(this.$control).closest('.selectize-control').addClass('_active');
                }
            },
            onFocus: function onFocus() {
                $(this.$control).closest('.selectize-control').addClass('_active');
            },
            onBlur: function onBlur() {
                if (!$(this.$control).hasClass('full')) {
                    $(this.$control).closest('.selectize-control').removeClass('_active');
                }
            }
        });

        var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

        $('.selectize-input input').attr('readonly', 'readonly');

        if (iOS) {
            $('body').on('click touchend', '.option', function (e) {
                var _this = this;

                if (!$(this).closest('[data-selectize-id]').length) {
                    return;
                }

                setTimeout(function () {
                    $select[Number($(_this).closest('[data-selectize-id]').data('selectize-id')) - 1].selectize.setValue($(_this).text(), false);
                }, 0);
            });
        }

        $(document).on('click', '.js-reset-filter', function () {

            $select.filter('.js-filter-selectize').each(function () {
                this.selectize.clear();

                $(this.selectize.$control).closest('.selectize-control').removeClass('_active');
            });
            $(this).removeClass("active");
        });
    }
}
function collectionsSlider() {
    var $collectionsSlider = $('.js-collections-slider');
    var scrollbarExists = true;

    if ($collectionsSlider.length) {
        var collectionsSliderUpdate = function collectionsSliderUpdate() {

            var activeCls = 'is-active';
            var $collectionsSliderBtn = $('.js-collections-slider-btn');

            if ($(window).width() <= 1024 && scrollbarExists) {
                // Дестрой скроллбара
                ps.destroy();
                ps = null;

                scrollbarExists = false;

                return;
            }

            if ($(window).width() > 1024) {

                if (!scrollbarExists) {
                    ps = new PerfectScrollbar($collectionsSlider[0], scrollbarOptions);
                    scrollbarExists = true;
                }

                ps.update();

                if ($collectionsSlider.hasClass('ps--active-x')) {
                    $collectionsSliderBtn.addClass(activeCls);
                } else {
                    $collectionsSliderBtn.removeClass(activeCls);
                }
            }
        };

        // Скролл по навигации


        var scrollbarOptions = {
            suppressScrollY: true
        };

        // Скроллбар на меню
        var ps = new PerfectScrollbar($collectionsSlider[0], scrollbarOptions);
        var isClicked = null;
        var coordX;
        var currentScrollLeft;

        // Драг меню
        $collectionsSlider.on('mousedown', function (e) {
            e.preventDefault();

            coordX = e.pageX;
            currentScrollLeft = $collectionsSlider[0].scrollLeft;

            isClicked = true;
        });

        $('body').on('mousemove', function (e) {
            if (isClicked) {
                $collectionsSlider[0].scrollLeft = currentScrollLeft - (e.pageX - coordX);
            }
        }).on('mouseup', function () {
            isClicked = false;
        });

        $(window).resize(throttle(collectionsSliderUpdate, 50));
        $(window).trigger('resize');

        var isAnimated = false;

        $('.js-collections-slider-btn').on('click', function (e) {
            e.preventDefault();

            if (isAnimated) {
                return;
            }

            var $this = $(this);
            var offset = 0;

            if ($this.hasClass('is-next')) {
                offset = 150;
            } else if ($this.hasClass('is-prev')) {
                offset = -150;
            }

            isAnimated = true;

            $collectionsSlider.stop(true, true).animate({ scrollLeft: $collectionsSlider[0].scrollLeft + offset }, 300, function () {
                isAnimated = false;
            });
        });
    }
}
var charactersTableHeight;
var animationDuration = 600;

function collectionCharacters() {
    if ($('.js-characters-table-wrap').length) {
        $('.js-characters-table-wrap').each(function () {
            var $innerContent = $(this).find('.js-characters-table-wrap-inner');

            // Минус единица здесь костыль, проблемы с округлением
            if ($innerContent.get(0).scrollHeight - 1 > parseInt($innerContent.css('height'))) {
                $(this).find('.js-toggle-characters').addClass('is-active');
            }
        });
    }
}

$(document).on('click', '.js-toggle-characters', function () {
    var $this = $(this);
    var $parent = $this.closest('.js-characters-table-wrap');
    var $innerContent = $parent.find('.js-characters-table-wrap-inner');

    if (!charactersTableHeight) {
        charactersTableHeight = parseInt($innerContent.css('height'));
    }

    $parent.toggleClass('is-full');

    $innerContent.animate({ height: parseInt($innerContent.height()) > charactersTableHeight ? charactersTableHeight : $innerContent.get(0).scrollHeight }, animationDuration);
});

$(document).on('click', '.js-toggle-stove-combinations', function () {
    var $parent = $(this).closest('.js-stove-combinations-wrap');

    $parent.toggleClass('is-full').find('.js-stove-combinations-wrap-inner').slideToggle(animationDuration, function () {
        $parent.get(0).scrollIntoView({ behavior: 'smooth', block: 'center' });
    });
});

function validateForm() {
    var validateOpts = {
        errorClass: '_error',
        errorPlacement: function errorPlacement(error, element) {
            var $formGroup = element.closest('.form-group');

            if ($formGroup.length) {
                $formGroup.addClass('_error');
            }
        },
        unhighlight: function unhighlight(element) {
            $(element).closest('.form-group').removeClass('_error');
        },
        submitHandler: function submitHandler(form, event) {
            event.preventDefault();

            if ($(form).is('.js-validate-form')) {

                $.ajax({
                    url: $(form).data('ajax-url'),
                    data: $(form).serialize(),
                    dataType: 'json',
                    success: function success(resp) {
                        if (resp.res == true) {
                            form.reset();

                            if ($('.setsEmptyPopup').length) {
                                $.magnificPopup.open({
                                    items: {
                                        src: $('.setsEmptyPopup')
                                    },
                                    mainClass: 'mfp-fade',
                                    type: 'inline'
                                });
                            }
                        } else {
                            alert(resp.error);
                        }
                    }
                });
            }

            if ($(form).is('.js-design-form')) {
                var formData = new FormData(form);

                $.ajax({
                    url: $(form).data('ajax-url'),
                    data: formData,
                    method: 'POST',
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function success(resp) {
                        if (resp.res == true) {
                            form.reset();

                            $(form).find('.js-load-file-label').text('Прикрепить файл');

                            if ($('.setsEmptyPopup').length) {
                                $.magnificPopup.open({
                                    items: {
                                        src: $('.setsEmptyPopup')
                                    },
                                    mainClass: 'mfp-fade',
                                    type: 'inline'
                                });
                            }
                        } else {
                            alert(resp.error);
                        }
                    }
                });
            }
        }
    };

    $('.js-validate-form').validate(validateOpts);
    $('.js-design-form').validate(validateOpts);
}
function map() {
    $(document).on('click', '._js-change-coordinates', function () {
        var $this = $(this),
            coordinates = $this.data('coordinates').split(','),
            text = $this.data('description'),
            resultCords = [parseFloat(coordinates[0]), parseFloat(coordinates[1])];

        APP.placemark.geometry.setCoordinates(resultCords);
        APP.map.setCenter(resultCords);
        APP.map.container.fitToViewport();
        $('._js-map-description').text(text);
    });

    if ($(document).find('#map_area').length) {
        ymaps.ready(function () {
            APP.map = new ymaps.Map('map_area', {
                center: [55.751574, 37.613856],
                zoom: 12,
                controls: []
            }, {
                searchControlProvider: 'yandex#search'
            });

            var ZoomLayout = ymaps.templateLayoutFactory.createClass("<div>" + "<div id='zoom-in' class='zoom-in'>+</div>" + "<div id='zoom-out' class='zoom-out'>-</div>" + "</div>", {

                build: function build() {
                    ZoomLayout.superclass.build.call(this);

                    this.zoomInCallback = ymaps.util.bind(this.zoomIn, this);
                    this.zoomOutCallback = ymaps.util.bind(this.zoomOut, this);

                    $('#zoom-in').bind('click', this.zoomInCallback);
                    $('#zoom-out').bind('click', this.zoomOutCallback);
                },

                clear: function clear() {
                    $('#zoom-in').unbind('click', this.zoomInCallback);
                    $('#zoom-out').unbind('click', this.zoomOutCallback);

                    ZoomLayout.superclass.clear.call(this);
                },

                zoomIn: function zoomIn() {
                    APP.map = this.getData().control.getMap();
                    APP.map.setZoom(APP.map.getZoom() + 1, { checkZoomRange: true });
                },

                zoomOut: function zoomOut() {
                    APP.map = this.getData().control.getMap();
                    APP.map.setZoom(APP.map.getZoom() - 1, { checkZoomRange: true });
                }
            }),
                zoomControl = new ymaps.control.ZoomControl({ options: { layout: ZoomLayout } });

            APP.map.controls.add(zoomControl);

            APP.placemark = new ymaps.Placemark(APP.map.getCenter(), {}, {
                iconLayout: 'default#image',
                iconImageHref: '/local/templates/laminam/html/build/assets/svg_icons/map-marker.svg',
                iconImageSize: [41, 52],
                iconImageOffset: [-20, -26]
            });

            APP.map.geoObjects.add(APP.placemark);
        });
    }
}
function jump() {
    if ($(window).width() < 640) {
        return false;
    }

    $(document).on('click.links', '._js-link', function (e) {
        e.preventDefault();
        animationJump($(this).attr('href'));
    });

    function animationJump(link) {
        var changeHistoryState = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

        if (link.indexOf('#') === -1) {
            $.ajax({
                url: link,
                cache: false,
                success: function success(html) {
                    APP.ytPlayers = [];

                    var columns = new TimelineMax(),
                        title = $(html).filter('#title').text(),
                        wrapper = $(html).filter('#wrapper').children();

                    if (changeHistoryState) {
                        window.history.pushState({ 'pageTitle': title }, '', link);
                    }

                    $('#title').text(title);

                    $('.loader-page__content').html(wrapper.clone());
                    $('.loader-page__content').find('.site-header').css({
                        paddingRight: APP.scrollbarWidth
                    });

                    lazyLoadImg();
                    lazyLoadBackgroundImages();
                    loadYoutubeScript();
                    loadNewPage();

                    function loadNewPage() {
                        $('#wrapper').html(wrapper.clone());

                        animationAfterJump();
                        youtubeInit();
                        lazyLoadImg();
                        lazyLoadBackgroundImages();
                        slider();
                        selectize();
                        animateCollections();
                        collectionsSlider();
                        collectionCharacters();
                        validateForm();
                        form();
                        map();
                        projects();
                        setsOnVue();
                        animateScrollMagic();

                        $('.select').styler();

                        $('body').removeClass('_no-scroll').css({
                            paddingRight: 0
                        });

                        $('.site-header').css({
                            paddingRight: 0
                        });

                        $(window).scrollTop(0);

                        if (window.location.pathname === '/' || window.location.pathname === 'index.html' || window.location.pathname === 'index.php') {
                            $('body').removeClass('page');
                        } else {
                            $('body').addClass('page');
                        }
                    };
                }
            });
        }
    }

    $(window).on('popstate', function (e) {
        animationJump(window.location.pathname, false);
    });
}
var controller = void 0;

var isMobile = function (a) {
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) {
        $('body').addClass('is-mobile');return true;
    }
}(navigator.userAgent || navigator.vendor || window.opera);

function animateScrollMagic() {
    if (!isMobile) {
        controller = new ScrollMagic.Controller();

        // Плавное появление шапки
        new ScrollMagic.Scene({
            triggerElement: '.site-header__inner',
            triggerHook: 1
        }).setClassToggle('.site-header__inner', 'is-active').addTo(controller);

        // Смещение содержимого футера
        new ScrollMagic.Scene({
            triggerElement: '.site-footer__inner',
            triggerHook: 0.9,
            reverse: false
        }).setClassToggle('.site-footer__inner', 'is-active').addTo(controller);

        // Плавное появление блока и смещение внутренних элементов
        $('.fadeInTop').each(function () {
            var _this2 = this;

            var fadeInTop = TweenMax.to(this, 0.4, { opacity: 1, ease: Power2.easeInOut, paused: true });
            var itemHeight = $(this).outerHeight();
            new ScrollMagic.Scene({
                duration: itemHeight * 1,
                offset: itemHeight * 0.01,
                triggerElement: this,
                triggerHook: 0.5
            }).setTween(this, 0.5, {}).on('enter', function () {
                fadeInTop.play();

                requestAnimationFrame(function () {
                    $(_this2).find('.offsetInTop').each(function (index) {
                        var $self = $(this);

                        requestAnimationFrame(function () {
                            $self.addClass('is-active');
                        });
                    });
                });
            }).addTo(controller);
        });

        // Более плавное появление блока и смещение внутренних элементов
        $('.fadeInTopSlow').each(function () {
            var _this3 = this;

            var fadeInTopSlow = TweenMax.to(this, 0.4, { opacity: 1, ease: Power2.easeInOut, paused: true });
            var itemHeight = $(this).outerHeight();
            new ScrollMagic.Scene({
                duration: itemHeight * 1.5,
                offset: -itemHeight * 0.3,
                triggerElement: this,
                triggerHook: 0.5
            }).setTween(this, 0.5, {}).on('enter', function () {
                fadeInTopSlow.play();

                requestAnimationFrame(function () {
                    $(_this3).find('.offsetInTopSlow').each(function (index) {
                        var $self = $(this);

                        requestAnimationFrame(function () {
                            $self.addClass('is-active');
                        });
                    });
                });
            }).addTo(controller);
        });

        // Плавное появление блока
        $('.fadeInBlock').each(function () {
            var fadeInBlock = TweenMax.to(this, 0.4, { opacity: 1, ease: Power2.easeInOut, paused: true });
            var itemHeight = $(this).outerHeight();
            new ScrollMagic.Scene({
                duration: itemHeight * 1.5,
                offset: -itemHeight * 0.3,
                triggerElement: this,
                triggerHook: 0.5
            }).setTween(this, 0.5, {}).on('enter', function () {
                fadeInBlock.play();
            }).addTo(controller);
        });

        // Анимация границ кнопки
        $('._js-animate-link').each(function () {
            var _this4 = this;

            new ScrollMagic.Scene({
                triggerElement: this,
                triggerHook: 0.9
            }).on('enter', function () {
                $(_this4).find('.js-button-line').each(function () {
                    var $self = $(this);

                    requestAnimationFrame(function () {
                        $self.addClass('is-active');
                    });
                });
            }).addTo(controller);
        });
    }
}
$(function () {
    // Mobile menu
    $(document).on('click', '._js-toggle-menu', function () {
        $('.site-mobile').toggleClass('_active');

        if (!$('.site-mobile').hasClass('_active')) {
            $('body').removeClass('noScrollMob').css({
                paddingRight: 0
            });
        } else {
            $('body').addClass('noScrollMob').css({
                paddingRight: APP.scrollbarWidth
            });
        }
    });

    $(document).on('mouseup touched', function (e) {
        var $el = $('.site-mobile__inner');
        if (!$el.is(e.target) && $el.has(e.target).length === 0) {
            if ($('.site-mobile').hasClass('_active')) {
                $('.site-mobile').removeClass('_active');
                $('body').removeClass('noScrollMob').css({
                    paddingRight: 0
                });
            }
        }
    });

    $(document).on('click', '._js-animate-video', function () {
        $(this).closest('.info-block__video-container').addClass('_active');
    });

    var videoHeight = null;

    $(document).on('click', '._js-video-block', function () {
        var $this = $(this);
        var video = $this.find('._js-video')[0];

        $this.toggleClass('_played');
        $this.find('._js-video-content').toggleClass('is-hidden');

        if (video.paused) {
            var body = $('html, body');
            videoHeight = $this.height();

            video.play();
        } else {
            video.pause();
        }
    });

    $(document).on('click', '.js-extend-filter', function () {
        var fadeDuration = 300;

        $(this).toggleClass('is-filter');

        $('.js-collections-slider-wrap').toggleClass('is-active');
        $('.js-catalog-filter-wrap').toggleClass('is-active');
    });

    $(document).on('change', '[data-collections-filter]', function () {
        var emptyFilters = true;
        var $collectionsList = $('.js-collections-list');
        var $materialsList = $('.js-materials-list');
        var activeCls = 'is-active';
        var emptyCls = 'is-empty';
        var filterReset = $(".js-reset-filter");

        var $materialsItem = $('[data-materials-item]');
        var filterParams = {};

        //показываем кнопку сброса
        filterReset.addClass("active");

        $('[data-collections-filter]').each(function () {
            var $filter = $(this);

            if ($filter.val() !== '') {
                filterParams[$filter.data('collections-filter')] = $filter.val();
                emptyFilters = false;
            }

            if (emptyFilters && $collectionsList.length) {
                $collectionsList.addClass(activeCls);
                $materialsList.removeClass(activeCls + ' ' + emptyCls);
            } else {
                $collectionsList.removeClass(activeCls);
                $materialsList.addClass(activeCls);

                var $materialsItems = $('[data-materials-item]');
                var materialsItemsLength = $materialsItems.length;

                $materialsItems.each(function () {
                    var $this = $(this);
                    var isHidden = false;

                    for (var i in filterParams) {
                        if (!Array.isArray($this.data('materials-item')[i]) && $this.data('materials-item')[i] !== filterParams[i] || Array.isArray($this.data('materials-item')[i]) && !$this.data('materials-item')[i].includes(filterParams[i])) {
                            isHidden = true;
                        }
                    }

                    $this[isHidden ? 'addClass' : 'removeClass']('is-hidden');
                });

                $('.js-materials-list')[$materialsItems.filter('.is-hidden').length < materialsItemsLength ? 'removeClass' : 'addClass'](emptyCls);
            }
        });
    });

    $(document).on('click', '.js-collections-list-btn', function () {
        $(this).closest('.js-collections-list').addClass('show-all');
    });

    $(document).on('click', '._js-stop-video', function () {
        var idVideoEl = $(this).closest('.video-block').find('.video, .video-frame').attr('id');

        $(this).closest('.video-block').removeClass('_played _active');
        for (var ind = 0; ind < APP.ytPlayers.length; ind++) {
            if (APP.ytPlayers[ind].a.id == idVideoEl) {
                stopYTVideo(APP.ytPlayers[ind]);
                break;
            }
        }
    });

    $(document).on('click', '._js-play-yt-video', function () {
        var idVideoEl = $(this).closest('.video-block').find('.video, .video-frame').attr('id');

        $(this).closest('.video-block').addClass('_played');
        for (var ind = 0; ind < APP.ytPlayers.length; ind++) {
            if (APP.ytPlayers[ind].a.id == idVideoEl) {
                playYTVideo(APP.ytPlayers[ind]);
                break;
            }
        }
    });

    $(document).on('click', '._js-stop-yt-video', function () {
        var idVideoEl = $(this).closest('.video-block').find('.video, .video-frame').attr('id');

        $(this).closest('.video-block').removeClass('_played _active');
        for (var ind = 0; ind < APP.ytPlayers.length; ind++) {
            if (APP.ytPlayers[ind].a.id == idVideoEl) {
                stopYTVideo(APP.ytPlayers[ind]);
                break;
            }
        }
    });

    var imgData = $('[data-stove-params]').first().data('stove-params');

    $(document).on('change', '[data-stove-params]', function () {
        var $this = $(this);
        imgData = $this.data('stove-params');

        $('.js-load-file').val('');
        $('.js-load-file-label').text('Прикрепить файл');

        updateImgData('.js-data-img-src', '.js-data-img-width', '.js-data-img-height', '.js-data-img-size', imgData);
    });

    function updateImgData(imgSrcElem, imgWidthElem, imgHeightElem, imgSizeElem, data) {
        $(imgSrcElem).attr('src', data.src);
        $(imgWidthElem).text(data.width);
        $(imgHeightElem).text(data.height);
        $(imgSizeElem).text(data.size);
    }

    var URL = window.URL || window.webkitURL;

    $(document).on('change', '.js-load-file', function () {
        var $this = $(this);

        if (!window.FileReader) {
            alert('Загрузка файлов не поддерживается вашим браузером');

            return;
        }

        var file = $this[0].files[0];
        var fileSize = file.size / 1024 / 1024;

        if (fileSize > imgData.size) {
            alert('Файл больше ' + imgData.size + ' мб');
            $this.val('');
            return;
        }

        if (file.name.split('.').pop().toLowerCase() !== 'tif') {
            var image = new Image();

            image.onload = function () {
                if (this.width > imgData.width || this.height > imgData.height) {
                    alert('Размер до ' + imgData.width + 'x' + imgData.height + ' px');
                    $this.val('');
                    return;
                }

                $('.js-load-file-label').text(file.name);
            };

            image.src = URL.createObjectURL(file);
        } else {
            $('.js-load-file-label').text(file.name);
        }
    });

    $('.select').styler();

    tabs();
    form();
    modal();
    map();
    slider();
    selectize();
    animateCollections();
    collectionsSlider();
    collectionCharacters();
    validateForm();
    projects();
    setsOnVue();
    jump();
});

lazyLoadImg();
lazyLoadBackgroundImages();

function loadYoutubeScript() {
    if (typeof YT == 'undefined' || typeof YT.Player == 'undefined') {
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }
}

window.onYouTubePlayerAPIReady = function () {
    youtubeInit();
};

setTimeout(function () {
    loadYoutubeScript();
}, 2500);

$(window).on('load', function () {
    $('.js-site-preloader').addClass('is-hidden').on('transitionend', function () {
        requestAnimationFrame(animationForBlock);
        requestAnimationFrame(animateScrollMagic);
        requestAnimationFrame(function () {
            $('.animated._now').each(function () {
                var animationType = $(this).data('animate');

                $(this).addClass(animationType);
            });
        });
    });

    var linksArr = [];

    $('._js-link').not('[target="_blank"]').not('[href="#"]').not('[href=""]').each(function () {

        var href = $(this).attr('href');

        if (linksArr.indexOf(href) < 0 && href.indexOf('catalog') === -1) {
            linksArr.push(href);
        }
    });

    console.log(linksArr);

    var pattern = /(\/upload\/)([/|.|\w|\s|-])*\.(?:jpg|gif|png)/gmi;

    function loadPictures(key) {

        $.ajax({
            url: linksArr[key],
            success: function success(html) {
                var m;

                while ((m = pattern.exec(html)) !== null) {
                    if (m[0]) {
                        var img = new Image();
                        img.src = m[0];
                    }
                }

                if (key < linksArr.length) {
                    loadPictures(++key);
                }
            }
        });
    }

    if (window.matchMedia("(min-width: 768px)").matches) {
        loadPictures(0);
    }
});
// Functions
function scrollbarWidth() {
    var documentWidth = parseInt(document.documentElement.clientWidth);
    var windowsWidth = parseInt(window.innerWidth);
    var scrollbarWidth = windowsWidth - documentWidth;
    return scrollbarWidth;
}

function playYTVideo(e) {
    if (e.playVideo) {
        e.playVideo();
    }
}

function stopYTVideo(e) {
    e.stopVideo();
}

function stopAllVideo() {
    var idVideoEl = $('.video, .video-frame').attr('id');

    $('.video-block').removeClass('_played _active');
    for (var ind = 0; ind < APP.ytPlayers.length; ind++) {
        stopYTVideo(APP.ytPlayers[ind]);
    }
}

function animationForBlock() {
    $('._js-animated-block').each(function () {
        $(this).find('.animated').each(function () {
            var animationType = $(this).data('animate');

            $(this).addClass(animationType);
        });
    });
}

function animationElement() {
    $('.animated').each(function () {
        var animationType = $(this).data('animate');

        if ($(document).scrollTop() + $(window).height() / 1.3 > $(this).offset().top && $(document).scrollTop() - $(this).offset().top < $(this).height()) {
            $(this).addClass(animationType);
        }
    });
}

function animateCollections() {
    $('.js-collections-list-item').each(function (i, elem) {
        var tl = new TimelineLite({ paused: true });

        var $content = $(this).find('.js-collections-list-content');
        var $items = $(this).find('.js-stove-list-item');
        // var $stoveLayer = $(this).find('.js-stove-list-layer');

        // tl.to($content, 0.6, { ease: Power3.easeIn, x: 0 }).to($stoveLayer, 0.4, { height: 0, ease: Linear.easeNone});
        // $items.each(function(index, element) {
        // tl.set(element, { opacity: 0 });
        // });

        tl.to($content, 0.6, { ease: Circ.easeOut, x: 0 }).to($items, 0.35, { ease: Power4.easeOut, css: { opacity: 1, className: '+=is-active' } });

        elem.animation = tl;

        $(elem).on('mouseenter', function () {
            this.animation.play();
        }).on('mouseleave', function () {
            this.animation.reverse();
        });
    });
}

function animationAfterJump() {
    $(window).on('load scroll', function () {
        $(document).find('._js-animated-block').each(function () {
            if ($(document).scrollTop() + $(window).height() / 1.2 > $(this).offset().top && $(document).scrollTop() - $(this).offset().top < $(this).height()) {
                $(this).find('.animated').each(function () {
                    var animationType = $(this).data('animate'),
                        style = $(this).attr('style');

                    $(this).addClass(animationType).attr('style', (style !== undefined ? style + ';' : '') + '-webkit-animation-delay: 0s; animation-delay: 0s; -webkit-animation-duration: 0s; animation-duration: 0s; opacity: 1');
                });
            }
        });
    });
}

function lazyLoadImg() {
    $(document).on('ready', function () {
        $('[data-src]').each(function () {
            $(this).data('load', false);
        });
    });

    $(window).on('load scroll', function () {
        loadImg();
    });
}

function lazyLoadBackgroundImages() {
    $('[data-bgi]').each(function () {
        $(this).data('load', false);
    });

    $(window).on('load scroll', function () {
        loadBackgroundImages();
    });
}

function loadImg() {
    $('[data-src]').each(function () {
        var $this = $(this),
            src = $this.data('src');
        if ($(document).scrollTop() + $(window).height() * 1.5 > $this.offset().top && $(document).scrollTop() - $this.offset().top < $this.height() && !$this.data('load')) {
            $this.attr('src', src).data('load', true);
        }
    });
}

function loadBackgroundImages() {
    $('[data-bgi]').each(function () {
        var $this = $(this),
            src = $this.data('bgi');

        if ($(document).scrollTop() + $(window).height() > $this.offset().top && $(document).scrollTop() - $this.offset().top < $this.height() && !$this.data('load')) {
            $this.attr('style', 'background-image: url(' + src + ');').data('load', true);
        }
    });
}

function youtubeInit() {
    if (window.YT !== undefined) {
        $('.video, .video-frame').each(function (i, e) {
            var player = new YT.Player('video_' + $(e).data('video'), {
                height: '360',
                width: '640',
                videoId: $(e).data('video'),
                playerVars: {
                    'autoplay': 0,
                    'showinfo': 0,
                    'rel': 0,
                    'controls': 0,
                    'loop': 0,
                    'enablejsapi': 1,
                    'modestbranding': 0,
                    'playlist': $(e).data('video')
                }
            });
            APP.ytPlayers.push(player);
        });
    }
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            return false;
        }
    }
    return true;
}

function throttle(func, ms) {
    var isThrottled = false,
        savedArgs,
        savedThis;

    function wrapper() {
        if (isThrottled) {
            savedArgs = arguments;
            savedThis = this;
            return;
        }

        func.apply(this, arguments);

        isThrottled = true;

        setTimeout(function () {
            isThrottled = false;

            if (savedArgs) {
                wrapper.apply(savedThis, savedArgs);
                savedArgs = savedThis = null;
            }
        }, ms);
    }

    return wrapper;
}
function projects() {

    // slider with popup

    if ($('.project-slider').length) {
        $('.project-slider').slick({
            infinite: true,
            slidesToShow: 1,
            dots: false,
            arrows: true
        }).addClass('sliderVisible');

        var flag = false;
        $('.project-slider').on('afterChange', function (event, slick, currentSlide, nextSlide) {
            flag = true;
            window.currentSlide = currentSlide;
        });

        $('.project-slider__slide').magnificPopup({
            items: {
                src: '#sliderPopup',
                type: 'inline'
            },
            callbacks: {
                open: function open() {
                    var slider = $('.project-slider');

                    if (flag) {
                        slider[0].slick.slickGoTo(parseInt(currentSlide), true);
                    } else {
                        slider.slick('unslick').slick();
                    };
                },
                close: function close() {
                    $('.project-slider').slick('unslick').slick();
                    var currentSlide = 0;
                    flag = false;
                }
            },
            removalDelay: 300,
            mainClass: 'mfp-fade'
        });
    }

    // popup for send full set
    if ($('.setsFull').length) {
        $('.setsFull').magnificPopup({
            items: {
                src: '.setsFullPopup',
                type: 'inline'
            },

            mainClass: 'mfp-fade',
            removalDelay: 300,
            callbacks: {
                beforeOpen: function beforeOpen() {
                    jQuery('html').addClass('noScrollMob');
                },
                beforeClose: function beforeClose() {
                    jQuery('html').removeClass('noScrollMob');
                }
            }
        });
    }

    // popup for send set on email
    if ($('.setsEmailPopup').length) {
        $('.setsEmail').magnificPopup({
            items: {
                src: '.setsEmailPopup',
                type: 'inline'
            },
            mainClass: 'mfp-fade',
            removalDelay: 300,
            callbacks: {
                beforeOpen: function beforeOpen() {
                    jQuery('html').addClass('noScrollMob');
                },
                beforeClose: function beforeClose() {
                    jQuery('html').removeClass('noScrollMob');
                }
            }
        });
    }

    // popup for send single item
    if ($('.setsSinglePopup').length) {
        $('.setsSingle').magnificPopup({
            items: {
                src: '.setsSinglePopup',
                type: 'inline'
            },
            mainClass: 'mfp-fade',
            removalDelay: 300,
            callbacks: {
                beforeOpen: function beforeOpen() {
                    jQuery('html').addClass('noScrollMob');
                },
                beforeClose: function beforeClose() {
                    jQuery('html').removeClass('noScrollMob');
                }
            }
        });
    }

    // popup if sets is empty
    /*if ($('.setsEmptyPopup').length) {
    	$('#demoPopup').magnificPopup({ //on click demo button
    		  items: {
    		     	src: '.setsEmptyPopup', 
    		     	type: 'inline'
    		  },
    		  mainClass: 'mfp-fade',
    		  removalDelay: 300,
    		  closeOnContentClick: true,
    		  callbacks: {
                    beforeOpen: function() {
                        jQuery('html').addClass('noScrollMob');
                    },
                    beforeClose: function() {
                        jQuery('html').removeClass('noScrollMob');
                    }
                }
    	});
    }*/

    // popup if delete set
    /*$(document).on('click', '.setsDelete', function (){
    $('.setsDelete').magnificPopup({
     items: {
        	src: '.setsDeletePopup', 
        	type: 'inline'
     },
     mainClass: 'mfp-fade',
     removalDelay: 300,
    	  callbacks: {
                 beforeOpen: function() {
                     jQuery('html').addClass('noScrollMob');
                 },
                 beforeClose: function() {
                     jQuery('html').removeClass('noScrollMob');
                 }
             }
    });
    });*/

    // if ($('.setsDeletePopup').length) {
    //     $('.setsDelete').magnificPopup({
    //         items: {
    //             src: '.setsDeletePopup',
    //             type: 'inline'
    //         },
    //         mainClass: 'mfp-fade',
    //         removalDelay: 300,
    //         callbacks: {
    //             beforeOpen: function() {
    //                 jQuery('html').addClass('noScrollMob');
    //             },
    //             beforeClose: function() {
    //                 jQuery('html').removeClass('noScrollMob');
    //             }
    //         }
    //     });
    // }


    // css class for close current popup
    $(document).on('click', '.closePopup', function () {
        $.magnificPopup.instance.close();
    });

    // Form validation
    $('.form-submit').on('click', function (event) {
        var r = /^.+@\w+\.\w{2,12}$/i;
        // var open_popup_link = $(".form-submit-popup");
        // var open_popup_link_error = $(".form-submit-popup-error");
        event.preventDefault();
        var error = [];

        $.each($(this).closest('form').serializeArray(), function (index, data) {
            if (data.value == "") {
                //сперва проверка на пустоту
                $("." + data.name).parent().addClass("_error");
                error.push(data.name);
            } else {
                if (data.name == "mail") {
                    if (!r.test($("[name=" + data.name + "]").val())) {
                        $("." + data.name).parent().addClass("_error");
                        error.push(data.name);
                    } else {
                        $("." + data.name).parent().removeClass("_error");
                        if (error.indexOf(data.name) != "-1") error.splice(error.indexOf(data.name), 1);
                    }
                } else if (data.name == "phone") {
                    if ($("[name=" + data.name + "]").val().length == 15) {
                        $("." + data.name).parent().removeClass("_error");
                        if (error.indexOf(data.name) != "-1") error.splice(error.indexOf(data.name), 1);
                    } else {
                        $("." + data.name).parent().addClass("_error");
                        if (error.indexOf(data.name) == "-1") error.push(data.name);
                    }
                } else {
                    $("." + data.name).parent().removeClass("_error");
                    if (error.indexOf(data.name) != "-1") error.splice(error.indexOf(data.name), 1);
                }
            }
        });
        /*if(error.length == 0){
        	BX.ajax.get(
        		"/ajax/request.php?"+$(this).closest('form').serialize(),
        		function (result)
        		{
        			var new_result = JSON.parse(result);
        			if(new_result.res){
        				open_popup_link.click();
        				$('.form-control').val('');
        				$('.form-group').removeClass('_active');
        			}else{
        				open_popup_link_error.click();
        				// $('.js-text').html(",  Причина:"+new_result.reason);
        			}
        		}
        	);
        }*/
    });
}

// show message when form submitted
function showSubmitMessage() {
    document.querySelector('.form-message').classList.add('ShowMessage');
}

function setsOnVue() {
    var storageName = 'cart';

    var Cart = function () {
        function Cart() {
            _classCallCheck(this, Cart);

            this.items = [];
            this.load();
        }

        _createClass(Cart, [{
            key: 'save',
            value: function save() {
                window.localStorage.setItem(storageName, JSON.stringify(this.items));
            }
        }, {
            key: 'load',
            value: function load() {
                this.items = JSON.parse(window.localStorage.getItem(storageName) || '[]');
            }
        }, {
            key: 'createNewSet',
            value: function createNewSet(name) {
                this.items.push({ name: name, isEdit: false, ids: [] });
                this.save();
            }
        }, {
            key: 'removeSet',
            value: function removeSet(index) {
                this.items.splice(index, 1);
                this.save();
            }
        }, {
            key: 'removeItemFromSetByIndex',
            value: function removeItemFromSetByIndex(currentSetIndex, item_id) {
                if (!this.items[currentSetIndex]) {
                    return;
                }
                for (var i in this.items[currentSetIndex].ids) {

                    if (this.items[currentSetIndex].ids[i] == item_id) {
                        this.items[currentSetIndex].ids.splice(i, 1);
                        break;
                    }
                }
                this.save();
            }
        }, {
            key: 'addToCart',
            value: function addToCart(setName, id) {
                var isDefineFlag = false;

                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                    for (var _iterator = this.items[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                        var item = _step.value;

                        if (item.name == setName) {
                            isDefineFlag = true;
                            item.ids.push(id);
                            break;
                        }
                    }
                } catch (err) {
                    _didIteratorError = true;
                    _iteratorError = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }
                    } finally {
                        if (_didIteratorError) {
                            throw _iteratorError;
                        }
                    }
                }

                if (isDefineFlag == false) {
                    this.createNewSet(setName);
                    return this.addToCart(setName, id);
                }

                this.save();
            }
        }]);

        return Cart;
    }();

    var $card = new Cart();

    if (document.querySelector('#app')) {

        var cart = new Vue({
            delimiters: ['${', '}'],
            el: '#app',
            data: {
                name: '',
                newSet: false,
                create: false,
                items: [],
                dataSetIndex: 1000,
                showPopupFirstTime: 1,
                imgUrl: '/assets/catalogue/bianco.jpg',
                itemsSet: []
            },

            mounted: function mounted() {
                var _this5 = this;

                var flag = window.localStorage.getItem('showPopupFirstTime') || 1;

                if (flag == 1) {
                    $.magnificPopup.open({
                        items: {
                            src: '.setsEmptyPopup',
                            type: 'inline'
                        },
                        mainClass: 'mfp-fade',
                        removalDelay: 300,
                        closeOnContentClick: true,
                        callbacks: {
                            beforeOpen: function beforeOpen() {
                                jQuery('html').addClass('noScrollMob');
                            },
                            beforeClose: function beforeClose() {
                                jQuery('html').removeClass('noScrollMob');
                            }
                        }
                    });

                    window.localStorage.setItem('showPopupFirstTime', 0);
                }

                this.$el.classList.add('showVueBlock');
                this.items = $card.items.map(function (s) {
                    s.thumbs = '';
                    return s;
                });

                var thumbs = [];
                this.items.forEach(function (s) {
                    if (s.ids.length) {
                        thumbs.push(s.ids[s.ids.length - 1]);
                    }
                });

                fetch('/local/templates/laminam/ajax/ajax_for_mix.php?ids=' + thumbs.join(','), {
                    method: 'GET',
                    mode: 'cors'
                }).then(function (response) {
                    return response.json();
                }).then(function (json_response) {
                    var key = 0;
                    _this5.items.forEach(function (s, index) {
                        if (s.ids.length && json_response[key]) {
                            _this5.setThumbs(index, json_response[key].image);
                            key++;
                        }
                    });
                });
            },

            methods: {
                setThumbs: function setThumbs(index, image) {
                    var _this6 = this;

                    this.$nextTick(function () {
                        _this6.items[index].thumbs = image;
                    });
                },

                setFocus: function setFocus() {
                    var _this7 = this;

                    this.newSet = !this.newSet;
                    Vue.nextTick().then(function () {
                        return _this7.$refs.createName.focus();
                    });
                },

                createNewSet: function createNewSet() {

                    $card.createNewSet(this.name);
                    this.items = $card.items;
                    this.name = '';
                    this.newSet = false;
                    this.save();
                },

                removeSet: function removeSet(index) {
                    $card.removeSet(index);
                    this.items = $card.items;
                },

                setEdit: function setEdit(index) {
                    var _this8 = this;

                    this.items[index].isEdit = true;
                    this.newSet = !this.newSet;
                    Vue.nextTick().then(function () {
                        _this8.$refs.editName[0].focus();
                    });

                    // Vue.nextTick().then(() => this.$refs.editName.focus());
                },

                editSet: function editSet(index) {
                    this.items[index].isEdit = false;
                    $card.items = this.items;
                    $card.save();
                },
                save: function save() {
                    window.localStorage.setItem(storageName, JSON.stringify(this.items));
                },
                load: function load() {
                    this.items = JSON.parse(window.localStorage.getItem(storageName) || '[]');
                },
                addToSet: function addToSet(setName, id) {},

                // popup if delete set
                openPopup: function openPopup(index) {

                    this.dataSetIndex = index;

                    $('.setsDelete').magnificPopup({
                        items: {
                            src: '.setsDeletePopup',
                            type: 'inline'
                        },
                        mainClass: 'mfp-fade',
                        removalDelay: 300,
                        callbacks: {
                            beforeOpen: function beforeOpen() {
                                return jQuery('html').addClass('noScrollMob');
                            },
                            beforeClose: function beforeClose() {
                                return jQuery('html').removeClass('noScrollMob');
                            }
                        }
                    }).click();
                },

                approveRemove: function approveRemove() {
                    this.removeSet(this.dataSetIndex);
                    $.magnificPopup.instance.close();
                }
            }
        });
    }

    // Cart item


    if (document.querySelector('#appItem')) {

        var cartItem = new Vue({
            delimiters: ['${', '}'],
            el: '#appItem',
            data: {
                dataSetIndex: 1000,
                currentSetIndex: -1,
                test: 'Vue',
                items: []
            },

            mounted: function mounted() {
                var _this9 = this;

                this.currentSetIndex = parseInt(window.location.search.substr(1));
                var ids = $card.items[this.currentSetIndex] ? $card.items[this.currentSetIndex].ids : [];

                fetch('/local/templates/laminam/ajax/ajax_for_mix.php?ids=' + ids.join(','), {
                    method: 'GET',
                    mode: 'cors'
                }).then(function (response) {
                    return response.json();
                }).then(function (json_response) {
                    _this9.items = json_response;
                });
            },

            methods: {

                removeSet: function removeSet(id) {

                    for (var i in this.items) {

                        if (this.items[i].id == id) {
                            this.items.splice(i, 1);
                            break;
                        }
                    }

                    $card.removeItemFromSetByIndex(this.currentSetIndex, id);
                    $card.save();
                },

                // popup if delete 
                openPopup: function openPopup(id) {

                    this.dataSetIndex = id;

                    $('.setsDelete').magnificPopup({
                        items: {
                            src: '.setsDeletePopup',
                            type: 'inline'
                        },
                        mainClass: 'mfp-fade',
                        removalDelay: 300,
                        callbacks: {
                            beforeOpen: function beforeOpen() {
                                return jQuery('html').addClass('noScrollMob');
                            },
                            beforeClose: function beforeClose() {
                                return jQuery('html').removeClass('noScrollMob');
                            }
                        }
                    }).click();
                },

                // popup if delete set
                openPopupSend: function openPopupSend(index) {

                    this.dataSetIndex = index;

                    $('.setsSingle').magnificPopup({
                        items: {
                            src: '.setsSinglePopup',
                            type: 'inline'
                        },
                        mainClass: 'mfp-fade',
                        removalDelay: 300,
                        callbacks: {
                            beforeOpen: function beforeOpen() {
                                jQuery('html').addClass('noScrollMob');
                            },
                            beforeClose: function beforeClose() {
                                jQuery('html').removeClass('noScrollMob');
                            }
                        }
                    }).click();
                },

                approveRemove: function approveRemove() {
                    this.removeSet(this.dataSetIndex);
                    $.magnificPopup.instance.close();
                }

            }
        });
    }

    if (document.querySelector('#appCatalog')) {

        var cartItem = new Vue({
            delimiters: ['${', '}'],
            el: '#appCatalog',
            data: {
                newSet: false,
                create: false,
                test: 'Vue',
                name: '',
                newName: '',
                items: [],
                id: -1,
                hideList: false,
                dataSetIndex: 1000
            },
            mounted: function mounted() {
                var _this10 = this;

                this.id = parseInt(document.querySelector('[data-id]').getAttribute('data-id'));

                this.items = $card.items.map(function (s) {
                    s.disabled = s.ids.indexOf(_this10.id) > -1 ? true : false;
                    return s;
                });
            },
            methods: {
                createNewSet: function createNewSet() {

                    if (this.name) {
                        $card.createNewSet(this.name);
                        this.items = $card.items;
                        $card.addToCart(this.name, this.id);
                        this.newName = this.name;
                        event.target.blur();
                        this.name = '';
                    }
                },

                addToSet: function addToSet(item, setIndex) {
                    $card.addToCart(item.name, this.id);
                    item.disabled = true;
                    this.newName = item.name;
                },

                openPopupSend: function openPopupSend(index) {

                    this.dataSetIndex = index;

                    $('.setsSingle').magnificPopup({
                        items: {
                            src: '.setsSinglePopup',
                            type: 'inline'
                        },
                        mainClass: 'mfp-fade',
                        removalDelay: 300,
                        callbacks: {
                            beforeOpen: function beforeOpen() {
                                jQuery('html').addClass('noScrollMob');
                            },
                            beforeClose: function beforeClose() {
                                jQuery('html').removeClass('noScrollMob');
                            }
                        }
                    }).click();
                }

            }

        });
    }

    return $card;
}
//# sourceMappingURL=main.js.map
